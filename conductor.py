import pygame as pg

class Conductor:
    def __init__(self, ai_settings, screen, sb):
        self.ai_settings = ai_settings
        self.screen = screen
        self.screen_rect = screen.get_rect()
        self.text_color = (90, 190, 240)
        self.font = pg.font.SysFont(None, 36)
        self.sb = sb
        self.prep_name()

    def prep_name(self):
        name = self.ai_settings.name
        self.score_image = self.font.render(name, True, self.text_color, self.ai_settings.bg_color)
        self.score_rect = self.score_image.get_rect()
        self.score_rect.left = self.sb.high_score_rect.right +110
        self.score_rect.top = 20

    def show_name(self):
        self.screen.blit(self.score_image, self.score_rect)
        #self.screen.blit(self.txt_surface, (self.input_box.x+5, self.input_box.y+5))
        #pg.draw.rect(self.screen, self.color, self.input_box, 2)
