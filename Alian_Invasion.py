import pygame
from settings import Settings
from game_stats import GameStats
from scoreboard import Scoreboard
from conductor import Conductor
from button import Button
from ship import Ship
import game_functions as gf
from pygame.sprite import Group


# במקרה של סיום משחק למחוק את כל הכדורים

def run_game():
    pygame.init()  # יצירת חלון ריק
    ai_settings = Settings()  # מקבל את כל ההגדרות
    screen = pygame.display.set_mode((ai_settings.screen_width, ai_settings.screen_height))  # מקבל גודל מסך
    pygame.display.set_caption("Alian Invasion")

    play_button = Button(screen, "Play")  # אחראי על חזות כפתור ההפעלה
    stats = GameStats(ai_settings)  # איתחול משחק ?!!
    sb = Scoreboard(ai_settings, screen, stats)  # אחראי על חזות התוצאות והרמה
    cc = Conductor(ai_settings, screen, sb)
    ship = Ship(ai_settings, screen)  # אחראי על חזות התמונה של הספינה
    bullets = Group()  # רשימה המחזיקה את כל הכדורים
    aliens = Group()  # רשימה המחזיקה את החייזרים
    gf.create_fleet(ai_settings, screen, ship, aliens)  # יוצר צי של חייזרים בהתאם לגודל המסך

    while True:  # גורם למסך להישאר
        gf.check_events(ai_settings, screen, stats, sb, play_button, ship, aliens, bullets, cc)  # תגובה להקשות במקלדת ועכבר
        if stats.game_active:
            ship.update()  # עדכון מיקום הספינה (אפשרות לזוז)
            gf.update_bullets(ai_settings, screen, stats, sb, ship, aliens, bullets)  # מטפל בכל כדור בין שפגע בחייזר ובין שלא פגע
            gf.update_aliens(ai_settings, screen, stats, sb, ship, aliens, bullets)  # אם חייזר פגע בספינה או הגיע לתחתית המסך
        gf.update_screen(ai_settings, screen, stats, sb, ship, aliens, bullets, play_button, cc)


run_game()
