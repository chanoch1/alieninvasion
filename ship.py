import pygame
from pygame.sprite import Sprite

class Ship(Sprite):
    def __init__(self, ai_settings, screen):
        super(Ship, self).__init__()# מאתחל את המחלקה עם הירושה
        self.screen = screen
        self.ai_settings = ai_settings
        self.image = pygame.image.load('image/ship.bmp')
        self.rect = self.image.get_rect()# שומר את המיקום של התמונה
        self.screen_rect = screen.get_rect()#שומר את המיקום שך המסך
        self.rect.centerx = self.screen_rect.centerx# שם את התמונה במרכז
        self.rect.bottom = self.screen_rect.bottom# שם את התמונה בתחתית

        self.center = float(self.rect.centerx)# נותן לתמונה אפשרות לעמוד על נקודה X עשרונית

        self.moving_right = False
        self.moving_left = False

    def update(self):
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.center += self.ai_settings.ship_speed_factor
        if self.moving_left and self.rect.left > 0:
            self.center -= self.ai_settings.ship_speed_factor
        self.rect.centerx = self.center

    def blitme(self):
        self.screen.blit(self.image, self.rect)# טוען את התמונה לתוך המסך במיקום rect

    def center_ship(self):
        self.center = self.screen_rect.centerx