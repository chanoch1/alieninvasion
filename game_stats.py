class GameStats():
    def __init__(self, ai_settings):
        self.ai_settings = ai_settings
        self.reset_stats()
        self.game_active = False
        with open('big_score.txt', 'r') as f:
            big_score = int(f.read())
        self.high_score = big_score

    def reset_stats(self):
        self.ships_left = self.ai_settings.ship_limit
        self.score = 0# ציון
        self.level = 1# רמה
